# HTTPD Production

This repository contains configuration files, documentation and accompanying resources for setting up and running _HTTPD WebDAV_ in production.
HTTPD WebDAV describes using the [Apache HTTPD](https://httpd.apache.org/) webserver software for providing high-throughput WebDAV endpoints.
The information in this repository is targeted at operators who want to provide such endpoints.

The primary scenario is providing WebDAV endpoints on top of existing large-scale filesystems with a supporting Identity Provider which manages user identities in the filesystem.

## Configuration

Examples of running and details on the Apache HTTPD configuration are available in the [config](./config) directory.

## Documentation

There exists no dedicated prose documentation yet.

Browse the [docs](./docs) directory for assorted schema graphics and further pieces of information.

## Docker Images

The repository contains Docker Images for running HTTPD WebDAV (built in CI).

### `httpd-webdav`

HTTPD server with all binaries, see its README.

### `httpd-webdav-dockerfy`

HTTPD server with automated configuration generation via environment variables.

## Issues, Questions and Discussions

The GitLab issue system is in use to allow anybody to contribute by asking questions, pointing out issues and conducting discussions regarding use cases and technical implementation.

## Licensing

Except if explicitly noted otherwise, all content in this repository is licensed under the Apache License, Version 2.0 (see LICENSE file).
Detailed authorship information is part of the Git commit data or included in the files directly.

```
Copyright 2021-2023 The Authors

Licensed under the Apache License, Version 2.0 (the "License");
you may not use these files except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
