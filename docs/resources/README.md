# Resources

The graphics were created with [diagrams.net](https://www.diagrams.net/).

The XML files are the native representation created through "File" → "Save as..." and can be imported through "File" → "Open from" → "Device...".

Other formats may be derived from each diagram.

PNGs are exported page-by-page through "File" → "Export as" → "PNG..." with transparent background and zoom possibly adapted for better legibility (e.g. 150 %).

PDFs are exported page-by-page through "File" → "Export as" → "PDF..." with explicit selection of a single page, transparent background and crop enabled.
