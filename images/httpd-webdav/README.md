# `httpd-webdav`

`httpd-webdav` contains the HTTPD server with all binaries required to provide a WebDAV endpoint for existing filesystems.
It is the main image in this repository.

At the moment, the following is included:

 * All Apache HTTPD modules maintained in-tree.
   Most relevant:
   * mod_dav and mod_dav_fs providing a handler for the WebDAV HTTP methods and the filesystem backend for the former.
   * mod_auth_basic providing HTTP Basic Auth support.
   * mod_ldap and mod_authnz_ldap providing an authentication and authorisation backend based on LDAP.
   * Various modules providing core functionality and configuration directives...
 * mod_mpmitk_setuid with capabilities support.
   This provides the ability to setuid to a specified POSIX user per request and execute the request handler as this user.
   See the [mod-mpmitk-setuid](https://gitlab.hzdr.de/kit-scc-sdm/onlinestorage/httpd-webdav/mod-mpmitk-setuid) repository.
 * The [apache-scoreboard-tools](https://github.com/bodgit/apache-scoreboard-tools) `check_apache` and `collectd_apache`.
   These can read a `ScoreBoardFile` for extracting information about the state of HTTPD child processes, thereby useful for out-of-band monitoring (i.e. not performing a HTTP request to HTTPD).
 * A directory is prepared for containing the lock DB by `mod_dav_fs`, see the [`DAVLockDB` directive](https://httpd.apache.org/docs/2.4/mod/mod_dav_fs.html#davlockdb).
   `DAVLockDB /usr/local/apache2/var/dav/lockdb` should be specified in the HTTPD configuration.

No specific configuration is included, all files in this repository originate from the official `httpd` image on Docker Hub.

The image is built as `registry.hzdr.de/kit-scc-sdm/onlinestorage/httpd-webdav/production-config/httpd-webdav:latest`.
