#!/bin/bash
SERVERROOT="/usr/local/apache2"

echo "init certificates..."

# SS: damit dockerfy nicht abstuerzt
mkdir -p /var/log/letsencrypt
touch /var/log/letsencrypt/letsencrypt.log

# init only if https is used
if [ $WEB_PROTOCOL = "http" ]; then
	echo "Due to exclusive use of HTTP no certificates are required!"
	exit 0
fi

chmod +x ${SERVERROOT}/run-certbot.sh
	
# init only if lets-encrypt is running for the first time, if HOSTNAME was set and no external Certs existing
if [ -z "$HOSTNAME" ]; then
	echo "skipping certificates due to missing hostname!"
	exit 1
fi

if [ ! -z "$SSL_CERT_LOCATION" ] && [ ! -z "$SSL_KEY_LOCATION" ]; then
	# using external certs without automatic renewal
	echo "skipping letsencrypt due to external certificates..."
	if [ ! -f "$SSL_CERT_LOCATION" ] || [ ! -f "$SSL_KEY_LOCATION" ];then
		echo "missing external certificates!"
		exit 1
	fi
	ln -sf $SSL_CERT_LOCATION ${SERVERROOT}/localserver.pem
	ln -sf $SSL_KEY_LOCATION ${SERVERROOT}/localserver.key

elif ([ ! -d $LETSENCRYPT_HOME ] || [ ! -d $LETSENCRYPT_HOME/live ] || [ -z "$(ls -A $LETSENCRYPT_HOME/live)" ]); then
	# first start of lets-encrypt	
	# creating self-signed cert for first start of webserver
	if [ ! -f ${LETSENCRYPT_HOME}/self-signed.pem ];then
		echo "creating self-signed certificate for first startup..."
		openssl req -newkey rsa:4096 -nodes -sha256 -keyout ${LETSENCRYPT_HOME}/self-signed.key -x509 -days 3650 -out ${LETSENCRYPT_HOME}/self-signed.pem -subj "/C=DE/ST=/L=/O=/OU=/CN=${HOSTNAME}"
	else
		echo "using pre-created self-signed certificate..."
	fi
	ln -sf ${LETSENCRYPT_HOME}/self-signed.pem ${SERVERROOT}/localserver.pem
	ln -sf ${LETSENCRYPT_HOME}/self-signed.key ${SERVERROOT}/localserver.key

	if [ ! -z $WEBMASTER_MAIL ]; then
		# TODO trigger first run of run_certbot.sh	
		echo "creating cron job for first certificate renewal..."
		echo -e "* * * * * root ${SERVERROOT}/run-certbot.sh >> /var/log/letsencrypt/letsencrypt.log 2>&1\n" > /etc/cron.d/initial_certbot
		chmod +x ${SERVERROOT}/run-certbot.sh
		crontab /etc/cron.d/initial_certbot
	else
		echo "won't create cron job for first certificate renewal due to missing WEBMASTER_MAIL!"
	fi
	
else
	# pre-existing lets-encrypt certificates
	if [ ! -f $LETSENCRYPT_HOME/live/$HOSTNAME/fullchain.pem ] || [ ! -f $LETSENCRYPT_HOME/live/$HOSTNAME/privkey.pem ];then
		echo "missing pre-created letsencrypt certificates!"
		exit 1
	fi
	echo "using pre-existing letsencrypt certificates"
	#echo "$(ls -A $LETSENCRYPT_HOME)/live"
    	ln -sf $LETSENCRYPT_HOME/live/$HOSTNAME/fullchain.pem ${SERVERROOT}/localserver.pem
	ln -sf $LETSENCRYPT_HOME/live/$HOSTNAME/privkey.pem ${SERVERROOT}/localserver.key
fi

echo "certificate init finished"

exit 0
