#!/bin/bash

# because cron has no environment variables, we have to template this file
SSL_KEY_LOCATION="{{ .Env.SSL_KEY_LOCATION }}"
SSL_CERT_LOCATION="{{ .Env.SSL_CERT_LOCATION }}"
WEBMASTER_MAIL="{{ .Env.WEBMASTER_MAIL }}"
HOSTNAME="{{ .Env.HOSTNAME }}"
LETSENCRYPT_STAGING="{{ .Env.LETSENCRYPT_STAGING }}"

LETSENCRYPT_HOME="/etc/letsencrypt"
SERVERROOT="/usr/local/apache2"
WEBROOT="$LETSENCRYPT_HOME/webroot"

echo "starting certificate renewal..."

# TODO: Damit renew funktioniert, muss webroot methode verwendet werden (sonst wird port 80 beim renew belegt und Server muesste vorher beendet werden)
# renew verwendet automatisch die initial verwendete Methode

if [ ! -z "$SSL_CERT_LOCATION" ] && [ ! -z "$SSL_KEY_LOCATION" ]; then
	# using external certs without automatic renewal
	echo "skipping lets-encrypt due to external certificates..."

elif ([ ! -d $LETSENCRYPT_HOME ] || [ ! -d $LETSENCRYPT_HOME/live ] || [ -z "$(ls -A $LETSENCRYPT_HOME/live)" ]); then
	# first start of lets-encrypt
	# get certificates with webroot method, because our apache could allready run
	echo "starting automatic lets-encrypt renewal..."
	if [ -z $WEBMASTER_MAIL ]; then
		echo "Can not use Let's Encrypt to renew cerificates due to missing WEBMASTER_MAIL!"
		exit 1
	fi
	mkdir -p $WEBROOT
	if ([ "$LETSENCRYPT_STAGING" == "true" ]) then
	  echo "Using Let's Encrypt Staging environment..."
	  certbot certonly --staging -n --webroot -w $WEBROOT --agree-tos --email $WEBMASTER_MAIL --domains $HOSTNAME
	else
	  echo "Using Let's Encrypt Production environment..."
	  certbot certonly -n --webroot -w $WEBROOT --agree-tos --email $WEBMASTER_MAIL --domains $HOSTNAME
	fi
	echo -e "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n"
	if [ ! -f $LETSENCRYPT_HOME/live/$HOSTNAME/fullchain.pem ] || [ ! -f $LETSENCRYPT_HOME/live/$HOSTNAME/privkey.pem ];then
		echo "certificate renewal failed - Missing pem-files!"
		exit 1
	fi
    	ln -sf $LETSENCRYPT_HOME/live/$HOSTNAME/fullchain.pem ${SERVERROOT}/localserver.pem
	ln -sf $LETSENCRYPT_HOME/live/$HOSTNAME/privkey.pem ${SERVERROOT}/localserver.key

	
	# pick up new certificates...
	${SERVERROOT}/bin/apachectl graceful	
else
	# pre-existing lets-encrypt certificates
	# -> starting renewal
	echo "starting lets-encrypt renewal..."
	current_key=`md5sum ${SERVERROOT}/localserver.key | awk '{print $1}'`
	current_crt=`md5sum ${SERVERROOT}/localserver.pem | awk '{print $1}'`
	mkdir -p $WEBROOT
	#echo "$(ls -A $LETSENCRYPT_HOME)"
	certbot -q renew
	new_key=`md5sum ${SERVERROOT}/localserver.key | awk '{print $1}'`
	new_crt=`md5sum ${SERVERROOT}/localserver.pem | awk '{print $1}'`
	
	#echo "Current Cert: $current_crt"
	#echo "New Cert: $new_crt"
	#echo "Current Key: $current_key"
	#echo "New Key: $new_key"


	# only restart if certs were changed
	if ([ ! "$current_key" == "$new_key" ] || [ ! "$current_crt" == "$new_crt" ]); then
		echo "lets-encrypt certifcates updated - perform apache restart..."
		${SERVERROOT}/bin/apachectl graceful	
	else
		echo "no update of lets-encrypt certificates"
	fi
fi

# remove initial cronjob trigger
[ -f "/etc/cron.d/initial_certbot" ] && rm -f /etc/cron.d/initial_certbot
exit 0
