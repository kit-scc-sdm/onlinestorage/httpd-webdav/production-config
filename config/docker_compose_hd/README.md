# Docker Compose

This configuration provides a basic WebDAV setup, based on docker-compose.
It is also Docker-Swarm ready, to provide a scale-up service.

It includes
* LDAP Authentication
* automatic LetsEncrypt Certificates
* setting the maximum number of parallel request handling processes to the number of cores on the host

## Installation
* copy and updating `docker-compose.yml.tmpl` with your site-specific
  configurations for
  * Hostnames and Ports
  * LDAP Information
  * Certificates
* adding secret files/information into `config/files/secrets`
* updating Design and Layout files in `config/files/www_resources`
* building your site-specific image `docker-compose -f docker-compose.yml build`

If you use docker swarm, you have to push the image additionally to your local registry: `docker-compose -f docker-compose.yml push`

## Starting the Container

With docker-compose: `docker-compose -f docker-compose.yml up -d`

With Docker Swarm: `docker stack deploy -c docker-compose.yml <your stackname>`
