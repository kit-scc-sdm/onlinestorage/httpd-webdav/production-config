# Reference HTTPD Configuration

This directory contains a full set of configuration files for Apache HTTPD to serve as a WebDAV endpoint.

It is meant as documentation resource, capturing motivation for parameter choices and best practices.
For quickly getting started with a testing endpoint, the Dockerfy image is probably a better approach.

**WIP: Documentation of choice of parameters, impact, ...**

Default configuration values are omitted in most cases and are only specified if there is a high interest in changing the value.

## Alternatives

Many different ways of configuring an Apache HTTPD server are available.

The most important differences are described as part of the reference configuration.

Alternative blocks are marked with the following comment lines:

 * `# Alternative: Auto Index`
 * `# Alternative: not(Auto Index)`
 * `# End of alternative`

## Required Modules

Not all of these modules are required for every configuration.
See `modules.load`.

 * Modules bundled with HTTPD, see the [HTTPD docs on modules for details](https://httpd.apache.org/docs/2.4/mod/)
 * [mod_mpmitk_setuid](https://gitlab.hzdr.de/kit-scc-sdm/onlinestorage/httpd-webdav/mod-mpmitk-setuid)
 * [mod_auth_openidc](https://github.com/zmartzone/mod_auth_openidc) (RPM and DEB packages available as part of GitHub releases)
 * [mod_oauth2](https://github.com/zmartzone/mod_oauth2) (RPM and DEB packages available as part of GitHub releases)
   * Requires [liboauth2](https://github.com/zmartzone/liboauth2) (RPM and DEB packages available as part of GitHub releases)
 * [mod_resolve](https://gitlab.hzdr.de/kit-scc-sdm/onlinestorage/httpd-webdav/mod-resolve)

### Module Interoperability

The WebDAV set-up relies on several modules not part of the Apache HTTPD project.
These module are developed to work well with all other modules, but may nevertheless introduce restrictions on the use of other modules.

Generally, any Apache HTTPD configuration should also work when integrated into this base configuration.
When in doubt or encountering specific unexpected errors, create an issue to discuss your needs.

## Apache HTTPD Documentation

 * https://httpd.apache.org/docs/2.4/
 * https://httpd.apache.org/docs/2.4/mod/
 * https://httpd.apache.org/docs/2.4/mod/directives.html

## Configuration Files

Configuration files may contain secrets, e.g. an LDAP password, and thus should not be read-accessible by any user except root.
For simplicity, it is recommended to chown root:root and set permissions 750 on the `/etc/httpd` directory, preventing any non-root user from reading any configuration files.
The configuration is read once by the main HTTPD process during start-up, so the process still runs as root at this point.

## Virtual Hosts

Each authentication mechanism has its dedicated virtual host with a separate domain.
This way, a single server can serve all supported authentication mechanisms.
Authentication and access is only supported via HTTPS for security reasons, so there is also a virtual host providing a HTTP-to-HTTPS forward for each virtual host with authentication.
The HTTP virtual host may also be used to enable the Letsencrypt webroot method.

## Architecture

To be expanded in own document.

 * Requirements: File system, information of all users locally (i.e. integration with infrastructure proxy), LDAP Server/OIDC Provider
 * Architecture models for each authentication mechanism
 * Scaling: Multiple servers
   * DNS-round-robin over IPs
   * Sessions
   * Caches
   * Letsencrypt
