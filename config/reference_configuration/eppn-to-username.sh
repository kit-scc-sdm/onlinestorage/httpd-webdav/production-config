#!/bin/bash
# This script provides the entire concept of mapping the eppn (extracted from the token) to a username. However, the details depend on your infrastructure proxy configuration.

function is_uint() {
    [[ "$1" =~ ^[0-9]+$ ]]
}

REGAPP_BASE_URL="REDACTED"
REST_USER="rest"
REST_PASS="REDACTED"
# Service's ID at the RegApp
SERVICE_ID="REDACTED"

iss="$1"

if [ -z "$iss" ]; then
    echo "Missing command line arg no. 1 (ISS)" >&2
    exit 105
fi

if [[ $iss == "https://login.bwidm.de/oidc/realms/bwidm_de" ]]; then
    eppn="$2"    
    if [ -z "$eppn" ]; then
    	echo "Missing command line arg no. 2 (EPPN)" >&2
    	exit 157
    fi

    result="$(curl --max-time 2 --head --silent --user "$REST_USER:$REST_PASS" "$REGAPP_BASE_URL/rest/attrq/eppn/$SERVICE_ID/$eppn")"
    rc=$?
    if [[ "$rc" -ne 0 ]]; then
        echo "Exit code $rc from curl not expected (eppn=$eppn)" >&2
        exit 153
    fi

    status_code="$(sed -r '1 { s@^HTTP/[0-9.]+ ([0-9]+).*@\1@; q }' <<< "$result")"
    if ! is_uint "$status_code"; then
        echo "Failed to parse RegApp response (eppn=$eppn):" >&2
        head -n 1 <<< "$result" >&2
        exit 152
    fi

    case "$status_code" in
        200)
            # All good
        ;;
        401)
            echo "RegApp status $status_code (uid=$useruid): Login failed" >&2
            exit 6
        ;;
        402)
            echo "RegApp status $status_code (uid=$useruid): Service ID not valid" >&2
            exit 151
        ;;
        403)
            echo "RegApp status $status_code (uid=$useruid): No assertion resulted from the AttributeQuery" >&2
            exit 6
        ;;
        404)
            echo "RegApp status $status_code (uid=$useruid): User is not registered" >&2
            exit 6
        ;;
        500)
            echo "RegApp status $status_code (uid=$useruid): Misconfigured service" >&2
            exit 158
        ;;
        *)
            echo "RegApp status $status_code not recognised (uid=$useruid)" >&2
            exit 159
        ;;
    esac

    result="$(curl --max-time 2 --silent --user "$REST_USER:$REST_PASS" "$REGAPP_BASE_URL/rest/attrq/eppn/$SERVICE_ID/$eppn")"
    rc=$?
    if [[ "$rc" -ne 0 ]]; then
        echo "Exit code $rc from curl not expected (eppn=$eppn)" >&2
        #exit 15
	exit 161
    fi

    useruid="$(jq -r ".uidNumber" <<< "$result")"

    userinfo="$(getent passwd "$useruid")"
    rc=$?
    if [[ "$rc" -eq 2 ]]; then
        echo "User was not found in passwd database (useruid=$useruid)" >&2
        exit 7
    elif [[ "$rc" -ne 0 ]]; then
        echo "Exit code $rc from getent not expected (useruid=$useruid)" >&2
        #exit 15
	exit 162
    fi

    username="$(awk -F ":" '{print $1}' <<< "$userinfo")"

    echo "$username"
else
    uid="$3"
    if [ -z "$uid" ]; then
	echo "Missing command line arg no. 2 (UID)" >&2
	exit 168
    fi
    echo "$uid"
fi

exit 0
