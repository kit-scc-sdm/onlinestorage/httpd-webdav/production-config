# Config

Examples of configuration files for Apache HTTPD for providing WebDAV endpoints.

## Reference Configuration

The [reference_configuration](./reference_configuration) directory contains a full set of configuration files for HTTPD.
Here, the recommended configuration is collected and documented.
Other examples and deployment methods mostly rely on this configuration.

## Docker Compose

The [docker_compose_hd](./docker_compose_hd) directory contains a Docker Compose based setup for running a WebDAV container.

## WebDAV in Docker for CI

The `httpd-webdav` image is tested in GitLab CI with the `webdav` "configuration"/"bundle" image.
Its [configuration file](../images/configurations/webdav/httpd.conf) is rather small (though not minimal) and sets up `mod_dav` and `mod_mpmitk_setuid`.
